const canvas=document.getElementById('realpainter');
const ctx=canvas.getContext('2d');  
var placeX = document.getElementById('inX');
var placeY = document.getElementById('inY');
var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
var PushArray = new Array();
var step = -1;
let isText = false;

var font = '14px sans-serif',
    //fontcolor = 'red';
    hasInput = false;


function push(){
    step++;
    if(step < PushArray.length) {PushArray.length = step;}
    PushArray.push(document.getElementById('realpainter').toDataURL());
  //  console.log('push');
}

function clean(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function download(th){
    var img = canvas.toDataURL("img/jpg");
    th.herf = img;
}
var R;
var G;
var B;

function changeColor(){
     R = document.getElementById("red").value;
     G = document.getElementById("green").value;
     B = document.getElementById("blue").value;
    ctx.strokeStyle = "rgb("+R+","+G+","+B+")";
}



function RChange(val){
    document.getElementById("RValue").innerHTML = val;
    changeColor();
}
function GChange(val){
    document.getElementById("GValue").innerHTML = val;
    changeColor();
}
function BChange(val){
    document.getElementById("BValue").innerHTML = val;
    changeColor();
}

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
           // canvas.width = img.width;
           // canvas.height = img.height;
            ctx.drawImage(img,placeX.value,placeY.value);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);    
    push(); 
}


ctx.lineJoin='round';   //兩條線交會處產生圓形邊角
ctx.lineCap='round';    //筆觸預設為圓形
ctx.lineWidth=1;    //筆頭寬度



let isDrawing=false;    //是否允許繪製(是否是mousedown下筆狀態)
let isErasing = false;
let isRainbow = false;
let isPen = true;
let isRect = false;
let mouseUp = false;
let lastX=0;    //繪製時的起點座標
let lastY=0;    //繪製時的起點座標



function useErase(){
    isErasing = !isErasing;
}
function useRainbow(){
    isRainbow = !isRainbow;
    
}


function erase(){
    ctx.strokeStyle = "rgb(255,255,255)";
}

/*繪圖函數撰寫1*/
/*========== Events Binding ==========*/
canvas.addEventListener('mouseup', () => {
    isDrawing = false;
    mouseUp = true;
    push();
});
canvas.addEventListener('mouseout', () => isDrawing = false);
canvas.addEventListener('mousedown', (e) => {
    isDrawing = true;   //允許繪製
    [lastX, lastY]=[e.offsetX, e.offsetY];  //設定起始點
});

canvas.addEventListener('mousemove', draw);

/*========== 繪製函數；在 mousemove 的時候使用 ==========*/
//rectangle
function rect(fillFlag) {
    isPen = false;
    console.log("test");
    canvas.onmousedown = rectDown;
    canvas.onmouseup = rectUp;
    canvas.onmousemove = rectMove;
    var flag = false;
    function rectDown(e) {
        imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        startX = e.x - this.offsetLeft;
        startY = e.y - this.offsetTop;
        flag = true;
    }
    function rectUp(e) {
        flag = false;
    }
    function rectMove(e) {
        if (flag) {
            ctx.putImageData(imageData, 0, 0);
            rectWidth = (e.x - this.offsetLeft) - startX;
            rectHeight = (e.y - this.offsetTop) - startY;
           // console.log(fillFlag);
            if (fillFlag == 0) {
                ctx.strokeRect(startX, startY, rectWidth, rectHeight);
            } else {
                ctx.fillStyle = "rgb("+R+","+G+","+B+")"
                ctx.fillRect(startX, startY, rectWidth, rectHeight);
            }
        }
    }
}

//circle
function drawEllipse(x1, y1, x2, y2) {

    var radiusX = (x2 - x1) * 0.5,   /// radius for x based on input
        radiusY = (y2 - y1) * 0.5,   /// radius for y based on input
        centerX = x1 + radiusX,      /// calc center
        centerY = y1 + radiusY,
        step = 0.01,                 /// resolution of ellipse
        a = step,                    /// counter
        pi2 = Math.PI * 2 - step;    /// end angle

    /// start a new path
    ctx.beginPath();

    /// set start point at angle 0
    ctx.moveTo(centerX + radiusX * Math.cos(0),
               centerY + radiusY * Math.sin(0));

    /// create the ellipse    
    for(; a < pi2; a += step) {
        ctx.lineTo(centerX + radiusX * Math.cos(a),
                   centerY + radiusY * Math.sin(a));
    }

    /// close it and stroke it for demo
    ctx.closePath();
    
    ctx.stroke();
}
function circle(fillFlag) {
    isPen = false;
    console.log("test");
    canvas.onmousedown = circleDown;
    canvas.onmouseup = circleUp;
    canvas.onmousemove = circleMove;
    var flag = false;
    function circleDown(e) {
            imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        startX = e.x - this.offsetLeft;
        startY = e.y - this.offsetTop;
        flag = true;
    }
    function circleUp(e) {
        flag = false;
    }
    function circleMove(e) {
        if (flag) {
            ctx.putImageData(imageData, 0, 0);
            rectWidth = (e.x - this.offsetLeft);
            rectHeight = (e.y - this.offsetTop);
           // console.log(fillFlag);
            if (fillFlag == 0) {
                ctx.strokeStyle = "rgb("+R+","+G+","+B+")";
                drawEllipse(startX, startY, rectWidth, rectHeight);
            } else {
                ctx.fillStyle = "rgb("+R+","+G+","+B+")";
                drawEllipse(startX, startY, rectWidth, rectHeight);
                ctx.fill();
            }
        }
    }
}
//triangle
function drawTri(x1,y1,x2,y2){
    ctx.beginPath();
    ctx.moveTo(x1,y1);
    ctx.lineTo(x2,y1);
    ctx.lineTo((x2+x1)/2,y2);
    ctx.closePath();
    ctx.stroke();
}
function triangle(fillFlag) {
    isPen = false;
    //console.log("test");
    canvas.onmousedown = triDown;
    canvas.onmouseup = triUp;
    canvas.onmousemove = triMove;
    var flag = false;
    function triDown(e) {
        imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        startX = e.x - this.offsetLeft;
        startY = e.y - this.offsetTop;
        flag = true;
    }
    function triUp(e) {
        flag = false;
    }
    function triMove(e) {
        if (flag) {
            ctx.putImageData(imageData, 0, 0);
            rectWidth = (e.x - this.offsetLeft);
            rectHeight = (e.y - this.offsetTop);
           // console.log(fillFlag);
            if (fillFlag == 0) {
                ctx.strokeStyle = "rgb("+R+","+G+","+B+")";
                drawTri(startX, startY, rectWidth, rectHeight);
            } else {
                console.log("please");
                ctx.fillStyle = "rgb("+R+","+G+","+B+")";
                drawTri(startX, startY, rectWidth, rectHeight);
                ctx.fill();
            }
        }
    }
}
function Draw(){
    isPen = true;
}


function useTxt(){
    isDrawing = false;
    console.log(canvas.onclick);
    canvas.onclick = function(e) {
        if (hasInput) return;
        addInput(e.clientX, e.clientY);
    }
    
    console.log(canvas.onclick);
}
var input;
function addInput(x, y) {
    input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';

    input.onkeydown = handleEnter;

    document.body.appendChild(input);

    input.focus();

    hasInput = true;
    //document.body.removeChild(lastChild);
    console.log("test");
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        
        hasInput = false;
    }
 
}

function drawText(txt, x, y) {
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font;
    ctx.fillText(txt, x - 80, y - 40);
}



function draw(e){
    //canvas.onclick() = null;
    if(!isDrawing || !isPen)  {   
        return;
    }
    canvas.onclick = null;
    canvas.onmousedown = null;
    canvas.onmouseup = null;
    canvas.onmousemove = null;
    
    ctx.beginPath();    
    ctx.moveTo(lastX, lastY);   
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    
    [lastX, lastY]=[e.offsetX, e.offsetY]; 

    if(isErasing) erase();
    else {
        changeColor();
        if(isRainbow) rainbow();  
    }
}


let hue=0;  

function rainbow(){
    ctx.strokeStyle=`hsl(${hue}, 100%, 50%)`;   //透過context的strokeStyle設定筆畫顏色
    hue++;  //色相環度數更新
    if(hue>=360){
        hue=0;
    }
}


 function minus(){
     if(ctx.lineWidth > 1){
         ctx.lineWidth -= 5;
     }
 }
 function larger(){
     if(ctx.lineWidth < 100){
         ctx.lineWidth += 5;
     }
 }


function Undo(){
    if (step >= 0) {
        step--;
        var picture = new Image();
        picture.src = PushArray[step];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        picture.onload = function () { ctx.drawImage(picture, 0, 0); }
     //   console.log(PushArray);
    }
}

function Redo(){
    if (step < PushArray.length-1) {
        step++;
        var picture = new Image();
        picture.src = PushArray[step];
        picture.onload = function () { ctx.drawImage(picture, 0, 0); }
    }
}




